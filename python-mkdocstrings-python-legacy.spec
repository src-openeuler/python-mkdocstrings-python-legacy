%global pypi_name mkdocstrings-python-legacy

Name:  python-%{pypi_name}
Version:  0.2.3
Release:  1
Summary:  A legacy Python handler for mkdocstrings.

License:	ISC
URL:    	https://mkdocstrings.github.io/python-legacy
Source0:	%{url}/archive/v%{version}/%{pypi_name}-%{version}.tar.gz
Patch1:  0001-fix-pdm-pep517-to-pdm-backend.patch
BuildArch:	noarch

%description
A legacy Python handler for mkdocstrings.

%package -n python3-%{pypi_name}
Summary:	A legacy Python handler for mkdocstrings.
Provides:	python-%{pypi_name}

BuildRequires:	python3-devel
BuildRequires:	python3-setuptools
BuildRequires:  python3-setuptools_scm
BuildRequires:  python3-pip
BuildRequires:  python3-wheel
BuildRequires:  python3-pdm-backend

%description -n python3-%{pypi_name}
A legacy Python handler for mkdocstrings.

%prep
%autosetup -n %{pypi_name}-%{version}

%build
%pyproject_build

%install
%pyproject_install

%files -n python3-%{pypi_name}
%license LICENSE
%doc README.md
%{python3_sitelib}/mkdocstrings_python_legacy-*.dist-info/

%changelog
* Fri Aug 25 2023 mengzhaoa <mengzhaoa@isoftstone.com> - 0.2.3-1
- Init package.